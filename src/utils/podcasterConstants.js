export const HEADING_TITLE = 'Podcaster';
export const INPUT_SEARCH_PLACEHOLDER = 'Filter podcasts...';
export const PODCAST_CARD_SUBHEADING = 'Author:';
export const PODCAST_AUTHOR_SUBHEADING_ARTICLE = 'by';
export const PODCAST_DESCRIPTION_SUBHEADING = 'Description';
export const PODCAST_LIST_HEADING = 'Episodes:';
