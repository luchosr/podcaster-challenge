export const aDayHasPassed = (localStorageObjectString) => {
  const actualDate = Date.now();

  const pastDate = localStorage.getItem(localStorageObjectString)
    ? JSON.parse(localStorage.getItem(localStorageObjectString)).timeStamp
    : 0;

  const millisecondsDifference = actualDate - pastDate;

  const daysDifference = millisecondsDifference / (1000 * 60 * 60 * 24);

  return daysDifference >= 1;
};

export const transformDate = (dateString) => {
  const date = new Date(dateString);

  const day = date.getDate().toString();
  const month = (date.getMonth() + 1).toString();
  const year = date.getFullYear().toString();

  return `${day}/${month}/${year}`;
};

export const millisecondsToHourMinutesAndSeconds = (milliseconds) => {
  const totalSeconds = Math.floor(milliseconds / 1000);
  const hours = Math.floor(totalSeconds / 3600);
  const minutes = Math.floor((totalSeconds % 3600) / 60);
  const seconds = totalSeconds % 60;

  const hoursFormatted = hours.toString();
  const minutesFormatted = minutes.toString();
  const secondsFormatted = seconds.toString();

  return `${hoursFormatted}:${minutesFormatted}:${secondsFormatted}`;
};

export const formatPodcastData = (podcastInfo) => {
  return podcastInfo?.results.slice(1).map((podcast, i) => ({
    id: i + 1,
    title: podcast.trackName,
    date: transformDate(podcast.releaseDate),
    duration: millisecondsToHourMinutesAndSeconds(podcast.trackTimeMillis),
    trackId: podcast.trackId,
    collectionId: podcast.collectionId,
  }));
};

export const processEscapedTextForReact = (text) => {
  const textWithSpaces = text.replace(/&nbsp;/g, ' ');

  const urlRegex =
    /(https?:\/\/[^\s]+)|(\b(?:https?:\/\/)?(?:www\.)?\S+\.(?:net|com|fm|org|COM|link)\/?\S*\b)/g;
  const textWithLinks = textWithSpaces.replace(urlRegex, (url) => {
    if (url.startsWith('http')) {
      return `<a href="${url}" target="_blank" rel="noopener noreferrer">${url}</a>`;
    } else {
      return `<a href="http://${url}" target="_blank" rel="noopener noreferrer">${url}</a>`;
    }
  });

  return textWithLinks;
};
