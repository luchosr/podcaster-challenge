import { Outlet } from 'react-router-dom';

import PodcasterHeading from '../components/PodcasterHeading/PodcasterHeading';

const RootOutlet = () => {
  return (
    <>
      <PodcasterHeading />
      <Outlet />
    </>
  );
};

export default RootOutlet;
