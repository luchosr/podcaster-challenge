import { Outlet } from 'react-router-dom';
import SmoothRender from 'react-smooth-render';

import Podcast from '../../components/Podcast/Podcast';

import './PodcastRoot.styles.css';

const PodcastRoot = () => {
  return (
    <>
      <div className="podcastRootWrapper">
        <SmoothRender hidden={false} timing={400}>
          <Podcast />
        </SmoothRender>
        <Outlet />
      </div>
    </>
  );
};

export default PodcastRoot;
