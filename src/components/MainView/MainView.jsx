import { useEffect, useState } from 'react';

import ScaleLoader from 'react-spinners/ScaleLoader';

import { getFirst100Podcasts } from '../../services/Podcasts';
import { useLocalStorage } from '../../hooks/useLocalStorage';

import Card from '../Card/Card';

import { aDayHasPassed } from '../../utils/podcasterUtils';
import { INPUT_SEARCH_PLACEHOLDER } from '../../utils/podcasterConstants';

import './MainView.styles.css';

const MainView = () => {
  const [filterValue, setFilterValue] = useState('');
  const [loading, setLoading] = useState(false);
  const [first100Podcasts, setFirst100Podcasts] = useLocalStorage(
    'podcastList',
    ''
  );

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      try {
        const response = await getFirst100Podcasts();
        setFirst100Podcasts(JSON.parse(response.data.contents));
      } finally {
        setLoading(false);
      }
    };

    if (aDayHasPassed('podcastList')) {
      fetchData();
    }
  }, []);

  const filteredPodcasts = first100Podcasts?.feed?.entry?.filter(
    (entry) =>
      entry['im:artist'].label
        .toLowerCase()
        .includes(filterValue.toLowerCase()) ||
      entry['im:name'].label.toLowerCase().includes(filterValue.toLowerCase())
  );

  return (
    <>
      {loading ? (
        <div className="cardGrid__loader">
          <ScaleLoader color="#36d7b7" width={30} height={70} />
        </div>
      ) : (
        <div className="inputWrapper">
          <span className="inputWrapper__counter">
            {filteredPodcasts?.length}
          </span>
          <input
            className="inputWrapper__input"
            type="text"
            value={filterValue}
            placeholder={INPUT_SEARCH_PLACEHOLDER}
            onChange={(event) => setFilterValue(event.target.value)}
          />
        </div>
      )}

      <div className="card-grid">
        {!loading
          ? filteredPodcasts?.map((entry) => (
              <Card
                podcastId={entry.id.attributes['im:id']}
                key={entry.id.attributes['im:id']}
                podcastAuthor={entry['im:artist'].label}
                podcastName={entry['im:name'].label}
                podcastDescription={entry.summary.label}
                imgUrl={entry['im:image'][2].label}
              />
            ))
          : ''}
      </div>
    </>
  );
};

export default MainView;
