import { Link } from 'react-router-dom';
import { useLocalStorage } from '../../hooks/useLocalStorage';

import './Card.styles.css';
import { PODCAST_CARD_SUBHEADING } from '../../utils/podcasterConstants';

const Card = ({
  imgUrl,
  podcastName,
  podcastAuthor,
  podcastId,
  podcastDescription,
}) => {
  // eslint-disable-next-line no-unused-vars
  const [podcastCardInfo, setPodcastCardInfo] = useLocalStorage(
    'podcastCardInfo',
    ''
  );

  return (
    <Link
      className="cardWrapper"
      to={`/podcasts/${podcastId}`}
      onClick={() =>
        setPodcastCardInfo({
          author: podcastAuthor,
          name: podcastName,
          description: podcastDescription,
          image: imgUrl,
        })
      }
    >
      <div className="cardWrapper__imageContainer">
        <img
          src={imgUrl}
          alt={podcastName}
          className="imageContainer__roundImage"
        />
      </div>
      <div className="cardWrapper__content">
        <h2 className="content__title">{podcastName}</h2>
        <p className="content__subTitle">
          {PODCAST_CARD_SUBHEADING} {podcastAuthor}
        </p>
      </div>
    </Link>
  );
};

export default Card;
