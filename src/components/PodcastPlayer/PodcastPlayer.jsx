import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

import { useLocalStorage } from '../../hooks/useLocalStorage';

import { processEscapedTextForReact } from '../../utils/podcasterUtils';

import './PodcastPlayer.styles.css';

const PodcastPlayer = () => {
  const [trackInfo, setTrackInfo] = useState([]);
  const params = useParams();
  // eslint-disable-next-line no-unused-vars
  const [podcastList, setPodcastList] = useLocalStorage(`${params.id}`, '');

  useEffect(() => {
    const podcastFormattedData = podcastList?.results
      .slice(1)
      .filter((podcast) => podcast.trackId === Number(params.episodeId));
    setTrackInfo(podcastFormattedData);
  }, []);

  console.log(trackInfo[0]?.description);
  return (
    <div className="podcastPlayerWrapper">
      <h2 className="podcastPlayerWrapper__title">{trackInfo[0]?.trackName}</h2>
      <div
        className="podcastPlayerWrapper__description"
        dangerouslySetInnerHTML={{
          __html: processEscapedTextForReact(`${trackInfo[0]?.description}`),
        }}
      />
      <audio className="audioControl" src={trackInfo[0]?.episodeUrl} controls />
    </div>
  );
};

export default PodcastPlayer;
