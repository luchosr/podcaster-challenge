import { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import DataTable from 'react-data-table-component';
import ScaleLoader from 'react-spinners/ScaleLoader';

import { getPodcastSpecificDetail } from '../../services/Podcasts';

import { useLocalStorage } from '../../hooks/useLocalStorage';

import {
  aDayHasPassed,
  millisecondsToHourMinutesAndSeconds,
  transformDate,
} from '../../utils/podcasterUtils';
import { PODCAST_LIST_HEADING } from '../../utils/podcasterConstants';

import './PodcastEpisodes.styles.css';

const PodcastEpisodes = () => {
  const [loading, setLoading] = useState(false);

  const [formattedData, setFormattedData] = useState([]);

  const params = useParams();
  const [podcastInfo, setPodcastInfo] = useLocalStorage(`${params.id}`, '');

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      try {
        await getPodcastSpecificDetail(`${params.id}`).then((response) =>
          formatEpisodesListData(JSON.parse(response.data.contents))
        );
      } finally {
        setLoading(false);
      }
    };

    if (aDayHasPassed(`${params.id}`)) {
      fetchData();
    } else {
      formatEpisodesListData(podcastInfo);
    }
  }, []);

  const formatEpisodesListData = (podcastData) => {
    setPodcastInfo(podcastData);
    const podcastFormattedData = podcastData?.results
      .slice(1)
      .map((podcast, i) => ({
        id: i + 1,
        title: podcast.trackName,
        date: transformDate(podcast.releaseDate),
        duration: millisecondsToHourMinutesAndSeconds(podcast.trackTimeMillis),
        trackId: podcast.trackId,
        collectionId: podcast.collectionId,
      }));
    setFormattedData(podcastFormattedData);
  };

  const columns = [
    {
      name: 'Title',
      width: '70%',
      selector: (row) => (
        <Link
          className="table__row"
          to={`/podcasts/${row.collectionId}/episode/${row.trackId}`}
        >
          {row.title}
        </Link>
      ),
    },
    {
      name: 'Date',
      selector: (row) => row.date,
    },
    {
      name: 'Duration',
      selector: (row) => row.duration,
    },
  ];

  const conditionalRowStyles = [
    {
      when: (row) => row.id % 2 > 0,
      style: {
        backgroundColor: '#fafafa',
        textDecoration: 'none',
      },
    },
  ];

  return (
    <>
      {loading ? (
        <div className="podcastEpisodesLoader">
          <ScaleLoader color="#36d7b7" width={30} height={70} />
        </div>
      ) : (
        <div className="podcastEpisodesWrapper">
          <div className="podcastEpisodes__titleCounter">
            <h2 className="heading__title">
              {PODCAST_LIST_HEADING} {podcastInfo?.resultCount - 1}
            </h2>
          </div>
          <div className="podcastEpisodes__tableWrapper ">
            <DataTable
              columns={columns}
              data={formattedData}
              conditionalRowStyles={conditionalRowStyles}
            />
          </div>
        </div>
      )}
    </>
  );
};

export default PodcastEpisodes;
