import { Link } from 'react-router-dom';

import { HEADING_TITLE } from '../../utils/podcasterConstants';
import './PodcasterHeading.styles.css';

const PodcasterHeading = () => {
  return (
    <>
      <div className="headingWrapper">
        <h1 className="headingWrapper__title">
          <Link className="headingWrapper__link" to="/">
            {HEADING_TITLE}
          </Link>
        </h1>
      </div>
    </>
  );
};

export default PodcasterHeading;
