import { useLocalStorage } from '../../hooks/useLocalStorage';

import {
  PODCAST_AUTHOR_SUBHEADING_ARTICLE,
  PODCAST_DESCRIPTION_SUBHEADING,
} from '../../utils/podcasterConstants';
import { processEscapedTextForReact } from '../../utils/podcasterUtils';

import './Podcast.styles.css';

const Podcast = () => {
  const [podcastCardInfo] = useLocalStorage('podcastCardInfo', '');

  return (
    <aside>
      <div className="podcastCardWrapper">
        <div className="podcastCardWrapper__imgContainer">
          <img
            src={podcastCardInfo.image}
            alt={`${podcastCardInfo.name}'s podcast`}
            className="imgContainer__img"
          />
        </div>
        <div className="podcastCardWrapper__titleText">
          <h2>{podcastCardInfo.name}</h2>
          <p>
            <i>
              {PODCAST_AUTHOR_SUBHEADING_ARTICLE} {podcastCardInfo.author}
            </i>
          </p>
        </div>
        <div className="podcastCardWrapper__descriptionText">
          <h4>{PODCAST_DESCRIPTION_SUBHEADING}</h4>
          <div
            className="descriptionText__subheading"
            dangerouslySetInnerHTML={{
              __html: processEscapedTextForReact(podcastCardInfo.description),
            }}
          />
        </div>
      </div>
    </aside>
  );
};

export default Podcast;
