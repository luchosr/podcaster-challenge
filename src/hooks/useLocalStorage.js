import { useState } from 'react';
export const useLocalStorage = (key, initialValue) => {
  const getStoredValue = () => {
    try {
      const item = window.localStorage.getItem(key);
      return item ? JSON.parse(item) : { value: initialValue, timeStamp: null };
    } catch (error) {
      return { value: initialValue, timeStamp: null };
    }
  };

  const [storedValue, setStoredValue] = useState(getStoredValue);

  const setValue = (value) => {
    try {
      const updatedValue = {
        value,
        timeStamp: Date.now(),
      };

      setStoredValue(updatedValue);

      window.localStorage.setItem(key, JSON.stringify(updatedValue));
    } catch (error) {
      console.error(
        'Local Storage could not been updated, the error is: ',
        error
      );
    }
  };

  return [storedValue.value, setValue, storedValue.timeStamp];
};

export default useLocalStorage;
