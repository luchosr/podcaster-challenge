import {
  createBrowserRouter,
  RouterProvider,
  createRoutesFromElements,
  Route,
  Outlet,
} from 'react-router-dom';
import RootOutlet from './router/RootOutlet';
import ErrorPage from './components/ErrorPage';

import MainView from './components/MainView/MainView';
import PodcastRoot from './router/PodcastRoot/PodcastRoot';
import PodcastEpisodes from './components/PodcastEpisodes/PodcastEpisodes';
import PodcastPlayer from './components/PodcastPlayer/PodcastPlayer';

import './App.styles.css';

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path="/" element={<RootOutlet />} errorElement={<ErrorPage />}>
      <Route index element={<MainView />} />
      <Route path="/podcasts" element={<PodcastRoot />}>
        <Route path=":id" element={<PodcastEpisodes />} />
        <Route path=":id/episode" element={<Outlet />}>
          <Route path=":episodeId" element={<PodcastPlayer />} />
        </Route>
      </Route>
    </Route>
  )
);

const App = () => {
  return (
    <>
      <RouterProvider router={router} />
    </>
  );
};

export default App;
