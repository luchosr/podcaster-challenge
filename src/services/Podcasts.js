import axios from 'axios';
const baseUrl = `https://api.allorigins.win/get?url=${encodeURIComponent(
  'https://itunes.apple.com/us/rss/toppodcasts/limit=100/genre=1310/json'
)}`;

export const getFirst100Podcasts = () => {
  return axios.get(baseUrl);
};

export const getPodcastSpecificDetail = (podcastId) => {
  return axios.get(
    `https://api.allorigins.win/get?url=${encodeURIComponent(
      `https://itunes.apple.com/lookup?id=${podcastId}&media=podcast&entity=podcastEpisode&limit=20`
    )}`
  );
};
