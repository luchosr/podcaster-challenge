<h1 align="center">Podcaster Challenge</h1>

<h6 align="center">
A Single Page Application made with Javascript and React.js
</h6>


## Description
Welcome to my project! This is a Podcaster Challenge,  made in  React.js using React Router V6 for routing, react-data-table for rendering data, react-smooth-render and react-spinners for a better experience, and eslint/prettier for code formatting. You can enjoy a podcast from the  100 most popular ones and get information about their producers.

### Instructions

You need to have node.js and npm installed with a preferable LTS version.

First, clone the repo:

```bash
> git clone https://gitlab.com/luchosr/podcaster-challenge.git
```


 once you cloned the repository and cd into it yo will need to install the dependencies


```bash
> npm install
```

with all the dependencies correctly installed, now you can run the app


```bash
> npm run dev
```
or if you want to run a production preview:

```bash
> npm run preview
```

then you will need to open a web browser tab with the following URL:

  Local:            http://localhost:5173/
 


Also, you can access the SPA deployment [HERE](https://podcaster-luchosr.netlify.app/).


![Podcaster Mainview Visual Preview](src/assets/images/main-picture.png)